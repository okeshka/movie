import {API_KEY} from '../constants/constants';
import BaseRequest from './baseService';

async function favouriteService() : Promise <HTMLElement[]> {
    let likeCards: HTMLElement[] = []; 
    if (localStorage.getItem('like')) {
        const likeArr :string[] = JSON.parse(localStorage.getItem('like') as string);
            for (const id of likeArr) {
                const element = <HTMLElement> <unknown>await new BaseRequest(`movie/${id}?api_key=${API_KEY}&language=en-US`).getCards(true);
                likeCards = [...likeCards, element];
            }
        return likeCards
    }
    throw new Error('you don`t like movies')
}

export {favouriteService};