import {API_KEY} from '../constants/constants';
import BaseRequest from './baseService';

async function popularService() : Promise <HTMLElement[]> {
    return await new BaseRequest(`movie/popular?api_key=${API_KEY}&language=en-US&page=1`).getCards()
}
export {popularService};