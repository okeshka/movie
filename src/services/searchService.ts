import {API_KEY} from '../constants/constants';
import BaseRequest from './baseService';

async function searchService (searchQuery: string) : Promise <HTMLElement[]> {
    return await new BaseRequest(`search/movie?api_key=${API_KEY}&language=en-US&page=1&include_adult=false&query=${searchQuery}`).getCards()
}

export {searchService};
