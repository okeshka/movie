import {API_KEY} from '../constants/constants';
import BaseRequest from './baseService';

async function upcomingService() : Promise <HTMLElement[]> {
    return new BaseRequest(`movie/upcoming?api_key=${API_KEY}&language=en-US&page=1`).getCards()
}

export {upcomingService};