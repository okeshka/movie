import {API_KEY} from '../constants/constants';
import BaseRequest from './baseService';

async function topService() : Promise <HTMLElement[]> {
    return await new BaseRequest(`movie/top_rated?api_key=${API_KEY}&language=en-US&page=1`).getCards()
}
export {topService};