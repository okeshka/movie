import card from '../components/card/card';
import favouriteMovies from '../components/card/favourites';
import {GET_URL} from '../constants/constants';

interface IMovie {
    poster_path: string;
    overview: string;
    release_date: string;
    id: number;
}

const mapper = (item: IMovie): HTMLElement => card(item.poster_path, item.overview, item.release_date, item.id);
const mapperFavourite = (item: IMovie): HTMLElement => favouriteMovies(item.poster_path, item.overview, item.release_date, item.id);


export default class BaseRequest {
    _url: string;
    constructor(url: string) {
        this._url = GET_URL + url;
    }
    async getCards(isId = false): Promise <HTMLElement[]>  {
        const response = await fetch(this._url);
        const data = await response.json();
        return isId ? mapperFavourite(data): data.results.map(mapper);
    }
}