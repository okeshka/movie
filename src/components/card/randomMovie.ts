export default function randomMovie(original_title: string, overview: string):  HTMLElement {
    const randomMovieWrapper: HTMLElement = document.createElement('div');
    randomMovieWrapper.classList.add('row', 'py-lg-5');
    randomMovieWrapper.innerHTML = `
        <div
            class="col-lg-6 col-md-8 mx-auto"
            style="background-color: #2525254f"
        >
            <h1 id="random-movie-name" class="fw-light text-light">${original_title}</h1>
            <p id="random-movie-description" class="lead text-white">
                ${overview}
            </p>
        </div>`;
    return randomMovieWrapper
}