import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './src/styles/styles.css';
//import { render } from './src/index';
import { searchService } from './src/services/searchService';
import { popularService } from './src/services/popularService'; 
import { upcomingService } from './src/services/upcomingService';
import { topService } from './src/services/topService';
import { favouriteService } from './src/services/favouriteService'

//render();
//localStorage.removeItem('like');
const searchButton = <HTMLButtonElement>document.getElementById('submit');
const filmContainer = <HTMLElement>document.getElementById('film-container');
const searchInput = <HTMLInputElement>document.getElementById('search');
const popularButton = <HTMLInputElement>document.getElementById('popular');
const upcomingButton = <HTMLInputElement>document.getElementById('upcoming');
const topButton = <HTMLInputElement>document.getElementById('top_rated');
const menuButton = <HTMLElement>document.querySelector('button.navbar-toggler');
const favouriteDiv = <HTMLElement>document.getElementById('favorite-movies');

//popular

function renderCards(cards: HTMLElement[]): void {
    filmContainer.append(...cards);
};

document.addEventListener("DOMContentLoaded", (): void => {
    popularService().then(renderCards)
    .then(() => {
        const allHearts: SVGElement[] = Array.from(document.getElementsByTagName('svg'));
        allHearts.forEach((svg: SVGElement) : void => svg
                  .addEventListener('click', () => {
                    svg.style.fill = "red";
                     const arrLike = JSON.parse(<string>localStorage.getItem('like'));
                     if (!arrLike) {
                            localStorage.setItem('like', JSON.stringify([svg.dataset.id]))
                        }
                        else {
                            if (!arrLike.includes(svg.dataset.id))
                            localStorage.setItem('like', JSON.stringify(arrLike.concat(svg.dataset.id)))
                        }
                    }))
    })
  });

popularButton.addEventListener("click", (): void => {
filmContainer.innerHTML ='';
popularService().then(renderCards)
});

//search
searchButton.addEventListener('click', (): void => {
    filmContainer.innerHTML ='';
    searchService(searchInput.value).then(renderCards)
});

//upcoming
upcomingButton.addEventListener("click", (): void => {
    filmContainer.innerHTML ='';
    upcomingService().then(renderCards);
});

//top
topButton.addEventListener("click", (): void => {
    filmContainer.innerHTML ='';
    topService().then(renderCards)
});

menuButton.addEventListener("click", (): void => {
    favouriteDiv.innerHTML ='';
    favouriteService().then((cards) => favouriteDiv.append(...cards))
});



